var color="black"
const reader = new FileReader();


function makegrid(x, y) {
	/*grid=document.getElementById("grille")
	for(var i=0; i<y;i++) {
		grid.innerHTML+="<tr>"
		for(var i=0; i<x;i++) {
			tr=document.getElementsByTagName("<tr>")
			let trO=tr[-1]
			trO.innerHTML+="<td></td>"
		}
		grid.innerHTML+="</tr>"
	}*/
	var body = document.getElementsByTagName("body")[0];

  // creates a <table> element and a <tbody> element
  var tbl = document.getElementById("pixels");
  var tblBody = document.createElement("tbody");

  // creating all cells
  for (var i = 0; i < y; i++) {
    // creates a table row
    var row = document.createElement("tr");

    for (var j = 0; j < x; j++) {
      // Create a <td> element and a text node, make the text
      // node the contents of the <td>, and put the <td> at
      // the end of the table row
      var cell = document.createElement("td");
      row.appendChild(cell);
    }

    // add the row to the end of the table body
    tblBody.appendChild(row);
  }

  // put the <tbody> in the <table>
  tbl.appendChild(tblBody);
  // appends <table> into <body>
  body.appendChild(tbl);
}
function show(data) {
	var table=document.getElementById("pixels");
	rows=table.rows;
	for(var i = 0; i < rows.length; i++){
		col=rows[i].cells;
		for(var x = 0; x < col.length; x++){
			color=data[i][x];
			col[x].className=color
		}
	}
}
function download(filename, text) {
    var pom = document.createElement('a');
    pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    pom.setAttribute('download', filename);

    if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
    }
    else {
        pom.click();
    }
}

function make(){
	makegrid(prompt("LARGEUR (en carreaux) : "), prompt("HAUTEUR (en carreaux) : "));
	$("td").addClass("white");
	$('td').on('click', function() {
    	if ($(this).hasClass('white')){
        	$(this).addClass(color).removeClass('white');}
    	else if ($(this).hasClass('black') || $(this).hasClass('red') || $(this).hasClass('green') || $(this).hasClass('blue')){
        	$(this).removeClass('red').removeClass('green').removeClass('blue').removeClass('black').addClass("white");}
	});
	$("#url").on("click", function() {navigator.clipboard.writeText(textarea_texte.value).then(() => {
                alert("URL de partage copié dans le presse-papiers")
            })});
        $("#print").on("click", function() {window.print()}); 
	$("#allblack").on("click", function() {$("td").removeClass("black").removeClass('white').removeClass('red').removeClass('green').removeClass('blue').addClass('black')});
	$("#allwhite").on("click", function() {$("td").removeClass("black").removeClass('white').removeClass('red').removeClass('green').removeClass('blue').addClass('white')});
	$("#allred").on("click", function() {$("td").removeClass("black").removeClass('white').removeClass('red').removeClass('green').removeClass('blue').addClass('red')});
	$("#allgreen").on("click", function() {$("td").removeClass("black").removeClass('white').removeClass('red').removeClass('green').removeClass('blue').addClass('green')});
	$("#allblue").on("click", function() {$("td").removeClass("black").removeClass('white').removeClass('red').removeClass('green').removeClass('blue').addClass('blue')});
	$("#red").on("click", function() {color="red";});
	$("#green").on("click", function() {color="green";});
	$("#blue").on("click", function() {color="blue";});
	$("#black").on("click", function() {color="black";});
	$("#replace").on("click", function() {
		var l1 = document.getElementById("colRp1");
		var v1 = l1.options[l1.selectedIndex].value;
		var l2 = document.getElementById("colRp2");
		var v2 = l2.options[l2.selectedIndex].value;
		console.log(v1+" "+v2)
		$(v1).removeClass('red').removeClass('green').removeClass('blue').removeClass('black').removeClass('white').addClass(v2);});
	$("#save").on("click", function() {
		var saved=[]
		var table=document.getElementById("pixels");
		rows=table.rows;
		for(var i = 0; i < rows.length; i++){
			saved.push([]);
			col=rows[i].cells;
			for(var x = 0; x < col.length; x++){
				cell=col[x];
				saved[saved.length-1].push(cell.className);
			}
		}
		download(prompt("Le nom de votre création :")+".pxl", JSON.stringify(saved));
	});
	$("#load").on("click", function() {
		var file=document.getElementById("fload").files[0];
		var res=document.getElementById("fstatus")
		console.log(file);
        var fr = new FileReader();
             
        fr.onprogress = function() {
            res.innerHTML = 'Chargement...';
        };
        fr.onerror = function() {
            res.innerHTML = 'Oups, une erreur s\'est produite...';
        };
        fr.onload = function() {
            res.innerHTML = '';
            var loaded=JSON.parse(fr.result)
			show(loaded);
        };
        
        fr.readAsText(file);
		
	});
	window.addEventListener('beforeunload', function (e) { 
        e.preventDefault(); 
        e.returnValue = ''; 
		
	});
	if(window.location.hash) {
    	// mettre le hash dans une variable et supprimer le caractère #
    	var hash = window.location.hash.substring(1);
		var decode=decodeURIComponent(hash);
    	try {
			show(JSON.parse(decode));
		} catch {
			alert("Une erreur s'est produite : Impossible de lire le paramètre");
	}
	}
		
}



$(make)
