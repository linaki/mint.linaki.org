#include <SPI.h>  // this forum strips off the angle brackets

// Connect to "Output SPI" JP4 on LED Matrix
// DIO 10 = CSn  (JP4-3)
// DIO 11 = MOSI (JP4-2)
// DIO 13 = SCK  (JP4-4)
//     5V = VCC  (JP4-5 or JP4-6)
//    GND = GND  (JP4-1)

const int CSn = 10;
const int ColorRED = 0x02;
const int ColorGREEN = 0x01;
const int ColorBLACK = 0x00;
const int ColorYELLOW = 0x03;
#define LEDS 256

int pixels[LEDS]={0};





char alphabet[27]="ABCDEFGHIJKLMNOPQRSTUVWXYZ ";
byte letters[27][8]={
{B00000000, B00111100, B01100110, B01100110, B01111110, B01100110, B01100110, B01100110},
{B01111000, B01001000, B01001000, B01110000, B01001000, B01000100, B01000100, B01111100},
{B00000000, B00011110, B00100000, B01000000, B01000000, B01000000, B00100000, B00011110},
{B00000000, B00111000, B00100100, B00100010, B00100010, B00100100, B00111000, B00000000},
{B00000000, B00111100, B00100000, B00111000, B00100000, B00100000, B00111100, B00000000},
{B00000000, B00111100, B00100000, B00111000, B00100000, B00100000, B00100000, B00000000},
{B00000000, B00111110, B00100000, B00100000, B00101110, B00100010, B00111110, B00000000},
{B00000000, B00100100, B00100100, B00111100, B00100100, B00100100, B00100100, B00000000},
{B00000000, B00111000, B00010000, B00010000, B00010000, B00010000, B00111000, B00000000},
{B00000000, B00011100, B00001000, B00001000, B00001000, B00101000, B00111000, B00000000},
{B00000000, B00100100, B00101000, B00110000, B00101000, B00100100, B00100100, B00000000},
{B00000000, B00100000, B00100000, B00100000, B00100000, B00100000, B00111100, B00000000},
{B00000000, B00000000, B01000100, B10101010, B10010010, B10000010, B10000010, B00000000},
{B00000000, B00100010, B00110010, B00101010, B00100110, B00100010, B00000000, B00000000},
{B00000000, B00111100, B01000010, B01000010, B01000010, B01000010, B00111100, B00000000},
{B00000000, B00111000, B00100100, B00100100, B00111000, B00100000, B00100000, B00000000},
{B00000000, B00111100, B01000010, B01000010, B01000010, B01000110, B00111110, B00000001},
{B00000000, B00111000, B00100100, B00100100, B00111000, B00100100, B00100100, B00000000},
{B00000000, B00111100, B00100000, B00111100, B00000100, B00000100, B00111100, B00000000},
{B00000000, B01111100, B00010000, B00010000, B00010000, B00010000, B00010000, B00000000},
{B00000000, B01000010, B01000010, B01000010, B01000010, B00100100, B00011000, B00000000},
{B00000000, B00100010, B00100010, B00100010, B00010100, B00010100, B00001000, B00000000},
{B00000000, B10000010, B10010010, B01010100, B01010100, B00101000, B00000000, B00000000},
{B00000000, B01000010, B00100100, B00011000, B00011000, B00100100, B01000010, B00000000},
{B00000000, B01000100, B00101000, B00010000, B00010000, B00010000, B00010000, B00000000},
{B00000000, B00111100, B00000100, B00001000, B00010000, B00100000, B00111100, B00000000},
{B00000000, B00000000, B00000000, B00000000, B00000000, B00000000, B00000000, B00000000}
/*{5, 126, 9, 9, 9, 126},
{5, 127, 73, 73, 73, 54},
{5, 62, 65, 65, 65, 65},
{5, 127, 65, 65, 65, 62},
{5, 127, 73, 73, 73, 65},
{5, 127, 9, 9, 9, 1},
{5, 62, 65, 65, 73, 121},
{5, 127, 8, 8, 8, 127},
{3, 65, 127, 65},
{5, 48, 65, 65, 65, 63},
{5, 127, 8, 20, 34, 65},
{5, 127, 64, 64, 64, 6},
{5, 127, 2, 12, 2, 127},
{5, 127, 4, 8, 16, 12},
{5, 62, 65, 65, 65, 6},
{5, 127, 9, 9, 9},
{5, 62, 65, 65, 97, 12},
{5, 127, 9, 25, 41, 7},
{5, 70, 73, 73, 73, 4},
{5, 1, 1, 127, 1, 1},
{5, 63, 64, 64, 64, 63},
{5, 31, 32, 64, 32, 31},
{5, 63, 64, 56, 64, 63},
{5, 99, 20, 8, 20, 99},
{5, 3, 4, 120, 4, 3},
{5, 97, 81, 73, 69, 67},
{3, 127, 65, 65}*/};

void setup()
{
    Serial.begin(9600);
    pinMode (CSn, OUTPUT);
    SPI.begin();
    SPI.setClockDivider(SPI_CLOCK_DIV128);
    SPI.setDataMode(SPI_MODE0);
    delay(1000);
    //displayText("KILI", 4, ColorGREEN, ColorBLACK, 3);
    //displayChar('K', ColorGREEN, ColorBLACK, 1, true);
    
}

int color=0;
int colors[]={ColorRED, ColorYELLOW};
int scroll=-70;

void loop()
{
    /*if (Serial.available()) {
      char letter=Serial.read();
      Serial.println(letter);
      displayChar(letter, ColorYELLOW, ColorBLACK);
      delay(1000);
    }*/
    //SetLED(ColorYELLOW);
    //delay(200);
    //SetLED(ColorRED);
    //delay(200);
    //SetLED(ColorGREEN);
    //delay(200);
    //
    /*for (int i=0;i<3;i++) {
      color=colors[i];
      for (int line=0;line<8;line++) {
        for (int row=0;row<8;row++) {
          setPixel(line, row, color);
          update();
          delay(100);
        }
      }
    }*/
    displayText("LINAKI MATRICES", 8, ColorGREEN, ColorBLACK, scroll);
    update();
    scroll++;
    delay(100);  
    
}

void SetLED(int color)
{
    digitalWrite(CSn,LOW);
    delay(100);
    for (int bytenum = 0; bytenum < 64; bytenum++) {
        SPI.transfer(color);
    }
    delay(100);
    digitalWrite(CSn,HIGH);
}

void displayText(char text[], int size, int color, int background, int offset) {
  for (int i=0;i<=size;i++) {
    displayChar(text[i], color, background, offset+i*8);
  }
}

void displayChar(char letter, int color, int background, int offset)
{
  for (int index=0;index<26;index++) {
    if (alphabet[index]==letter) {
      displayLetter(letters[index], color, background, offset);
    }
  }
}

void displayLetter(byte letter[], int color, int background, int offset)
{
    for (int line=0;line<8;line++) {
      for (int row=0;row<8;row++) {
        uint8_t bit=bitRead(letter[line], 8-row);
        if (bit==1) {
          setPixel(offset+row, line+1, color);
        }
        else {
          setPixel(offset+row, line+1, background);
        }
      }
    }
}

void setPixel(int x, int y, int color)
{
  if (x>=LEDS/64*8) {
    return;
  }
  int offset=(LEDS/64-((int)(x/8)))-1;
  y=y-(LEDS/64-offset);
  pixels[offset*64+x+y*8]=color;
}

void update() {
  digitalWrite(CSn, LOW);
  delay(10);
  for (int pixel=0;pixel<LEDS;pixel++) {
    SPI.transfer(pixels[pixel]);
  }
  digitalWrite(CSn, HIGH);
  delay(10);
}
