#include <SPI.h>  // this forum strips off the angle brackets
#include <string.h>

// Connect to "Output SPI" JP4 on LED Matrix
// DIO 10 = CSn  (JP4-3)
// DIO 11 = MOSI (JP4-2)
// DIO 13 = SCK  (JP4-4)
//     5V = VCC  (JP4-5 or JP4-6)
//    GND = GND  (JP4-1)

const int CSn = 10;
const int ColorRED = 0x02;
const int ColorGREEN = 0x01;
const int ColorBLACK = 0x00;
const int ColorYELLOW = 0x03;
#define LEDS 256

int pixels[LEDS]={0};





char alphabet[37]="ABCDEFGHIJKLMNOPQRSTUVWXYZ 1234567890";
byte letters[37][8]={
{B00000000, B00111100, B01100110, B01100110, B01111110, B01100110, B01100110, B01100110},
{B01111000, B01001000, B01001000, B01110000, B01001000, B01000100, B01000100, B01111100},
{B00000000, B00011110, B00100000, B01000000, B01000000, B01000000, B00100000, B00011110},
{B00000000, B00111000, B00100100, B00100010, B00100010, B00100100, B00111000, B00000000},
{B00000000, B00111100, B00100000, B00111000, B00100000, B00100000, B00111100, B00000000},
{B00000000, B00111100, B00100000, B00111000, B00100000, B00100000, B00100000, B00000000},
{B00000000, B00111110, B00100000, B00100000, B00101110, B00100010, B00111110, B00000000},
{B00000000, B00100100, B00100100, B00111100, B00100100, B00100100, B00100100, B00000000},
{B00000000, B00111000, B00010000, B00010000, B00010000, B00010000, B00111000, B00000000},
{B00000000, B00011100, B00001000, B00001000, B00001000, B00101000, B00111000, B00000000},
{B00000000, B00100100, B00101000, B00110000, B00101000, B00100100, B00100100, B00000000},
{B00000000, B00100000, B00100000, B00100000, B00100000, B00100000, B00111100, B00000000},
{B00000000, B00000000, B01000100, B10101010, B10010010, B10000010, B10000010, B00000000},
{B00000000, B00100010, B00110010, B00101010, B00100110, B00100010, B00000000, B00000000},
{B00000000, B00111100, B01000010, B01000010, B01000010, B01000010, B00111100, B00000000},
{B00000000, B00111000, B00100100, B00100100, B00111000, B00100000, B00100000, B00000000},
{B00000000, B00111100, B01000010, B01000010, B01000010, B01000110, B00111110, B00000001},
{B00000000, B00111000, B00100100, B00100100, B00111000, B00100100, B00100100, B00000000},
{B00000000, B00111100, B00100000, B00111100, B00000100, B00000100, B00111100, B00000000},
{B00000000, B01111100, B00010000, B00010000, B00010000, B00010000, B00010000, B00000000},
{B00000000, B01000010, B01000010, B01000010, B01000010, B00100100, B00011000, B00000000},
{B00000000, B00100010, B00100010, B00100010, B00010100, B00010100, B00001000, B00000000},
{B00000000, B10000010, B10010010, B01010100, B01010100, B00101000, B00000000, B00000000},
{B00000000, B01000010, B00100100, B00011000, B00011000, B00100100, B01000010, B00000000},
{B00000000, B01000100, B00101000, B00010000, B00010000, B00010000, B00010000, B00000000},
{B00000000, B00111100, B00000100, B00001000, B00010000, B00100000, B00111100, B00000000},
{B00000000, B00000000, B00000000, B00000000, B00000000, B00000000, B00000000, B00000000},
{B00000000,B00001000,B00011000,B00101000,B00001000,B00001000,B00001000,B00001000},
{B00000000,B00111100,B00000100,B00000100,B00111100,B00100000,B00100000,B00111100},
{B00000000,B00111100,B00000100,B00000100,B00011100,B00000100,B00000100,B00111100},
{B00000000,B00100100,B00100100,B00100100,B00111100,B00000100,B00000100,B00000100},
{B00000000,B00111100,B00100000,B00100000,B00111100,B00000100,B00000100,B00111100},
{B00000000,B00111100,B00100000,B00100000,B00111100,B00100100,B00100100,B00111100},
{B00000000,B00111100,B00000100,B00000100,B00000100,B00000100,B00000100,B00000100},
{B00000000,B00111100,B00100100,B00100100,B00111100,B00100100,B00100100,B00111100},
{B00000000,B00111100,B00100100,B00100100,B00111100,B00000100,B00000100,B00111100},
{B00000000,B00111100,B00100100,B00100100,B00100100,B00100100,B00100100,B00111100}};


void initMatrices() {
  for (int i=0;i<128;i++) {
    pixels[i*2]=ColorGREEN;
    pixels[i*2+1]=ColorRED;
    update();
  }
  for (int i=0;i<256;i++) {
    pixels[i]=ColorBLACK;
  }
  update();
}
void setup()
{
    Serial.begin(9600);
    pinMode (CSn, OUTPUT);
    SPI.begin();
    SPI.setClockDivider(SPI_CLOCK_DIV128);
    SPI.setDataMode(SPI_MODE0);
    delay(1000);
    update();
    initMatrices();
    
    //update();
    //displayText("KILI", 4, ColorGREEN, ColorBLACK, 3);
    //displayChar('K', ColorGREEN, ColorBLACK, 1, true);
    
}

int color=0;
int colors[]={ColorRED, ColorYELLOW};

//int scroll=-70;
int minute=-1;
int hour=22;
String minutes="00";
String hours="00";
String strHour="0000";
char charHour[5];

void loop()
{
    /*if (Serial.available()) {
      char letter=Serial.read();
      Serial.println(letter);
      displayChar(letter, ColorYELLOW, ColorBLACK);
      delay(1000);
    }*/
    /*SetLED(ColorYELLOW);
    delay(200);
    SetLED(ColorRED);
    delay(200);
    SetLED(ColorGREEN);
    delay(200);*/
    
    //
    /*for (int i=0;i<3;i++) {
      color=colors[i];
      for (int line=0;line<8;line++) {
        for (int row=0;row<8;row++) {
          setPixel(line, row, color);
          update();
          delay(100);
        }
      }
    }*/
    /*displayText("LINAKI MATRICES", 8, ColorGREEN, ColorBLACK, scroll);
    update();
    scroll++;
    delay(100);  */
    Serial.println(millis()/60000);
    if (millis()/60000>=minute+1) {
      minute=millis()/60000;
      if (minute==60) {
        hour++;
        minute=0;
      }
      if (hour==24) {
        hour=0;
      }
      minutes=String(minute);
      if (minute<10) {
        minutes="0"+minutes;
      }
      Serial.print("minutes : ");
      Serial.println(minutes);
      hours=String(hour);
      if (hour<10) {
        hours="0"+hours;
      }
      Serial.print("hours : ");
      Serial.println(hours);
      strHour=hours+minutes;
      Serial.println(strHour);
      strHour.toCharArray(charHour, sizeof(charHour));
      displayText(charHour, 4, ColorGREEN, ColorBLACK, 0);
      pixels[151]=ColorGREEN;
      ///pixels[159]=ColorGREEN;
      pixels[183]=ColorGREEN;
      //pixels[19]=ColorGREEN;
      update();
    }
    delay(2000);
    
    
}

void SetLED(int color)
{
    digitalWrite(CSn,LOW);
    delay(100);
    for (int bytenum = 0; bytenum < 64; bytenum++) {
        SPI.transfer(color);
    }
    delay(100);
    digitalWrite(CSn,HIGH);
}

void displayText(char text[], int size, int color, int background, int offset) {
  int rmOffset=0;
  for (int i=size;i>=0;i--) {
    if (i==1) {
      rmOffset=1;
    }

    else if (i==3) {
      rmOffset=1;
    }
    else if (i==2) {
      rmOffset=-1;
    }
    else {
      rmOffset=0;
    }
    displayChar(text[i], color, background, offset+i*8-rmOffset);
  }
}

void displayChar(char letter, int color, int background, int offset)
{
  for (int index=0;index<37;index++) {
    if (alphabet[index]==letter) {
      displayLetter(letters[index], color, background, offset);
    }
  }
}

void displayLetter(byte letter[], int color, int background, int offset)
{
    for (int line=0;line<8;line++) {
      for (int row=0;row<8;row++) {
        uint8_t bit=bitRead(letter[line], 8-row);
        if (bit==1) {
          setPixel(offset+row, line+1, color);
        }
        else {
          setPixel(offset+row, line+1, background);
        }
      }
    }
}

void setPixel(int x, int y, int color)
{
  x=32-x;
  x=x-1;
  if (x>=LEDS/64*8) {
    return;
  }
  int offset=(LEDS/64-((int)(x/8)))-1;
  y=y-(LEDS/64-offset);
  pixels[offset*64+x+y*8]=color;
}

void update() {
  digitalWrite(CSn, LOW);
  delay(10);
  for (int pixel=0;pixel<LEDS;pixel++) {
    SPI.transfer(pixels[pixel]);
  }
  digitalWrite(CSn, HIGH);
  delay(10);
}
